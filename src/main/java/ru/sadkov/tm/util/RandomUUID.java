package ru.sadkov.tm.util;

import java.util.UUID;

public class RandomUUID {

    public static String genRandomUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
