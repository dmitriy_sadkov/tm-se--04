package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.RandomUUID;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectServise {
    private ProjectRepository projectRepository = new ProjectRepository();
    private Scanner scanner;

    public ProjectServise(Scanner scanner) {
        this.scanner = scanner;
    }

    public String fingProjectIdByName(String projectName) {
        List<Project> projects = new ArrayList<>(projectRepository.findAll().values());
        String projectID = "";
        for (Project project : projects) {
            if (project.getName().equalsIgnoreCase(projectName)) {
                projectID = project.getId();
            }
        }
        return projectID;
    }

    public void saveProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
        } else {
            projectRepository.merge(new Project(projectName, RandomUUID.genRandomUUID()));
            System.out.println("[OK]");
        }
    }

    public void removeProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
        } else {
            projectRepository.remove(projectName);
            System.out.println("[OK]");
        }
    }

    public void showProjects() {
        if (projectRepository.isEmpty()) {
            System.out.println("[NO PROJECTS]");
        } else {
            int i = 1;
            for (Project project : projectRepository.findAll().values()) {
                System.out.println(i + ". " + project.getName());
            }
        }
    }

    public void clearProjects() {
        projectRepository.removeAll();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void update(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        String projectId = fingProjectIdByName(projectName);
        Project project = projectRepository.findAll().get(projectId);
        if (project == null) {
            System.out.println("[NO SUCH PROJECT]");
            return;
        }
        System.out.println("[ENTER NEW NAME]");
        String newName = scanner.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        String description = scanner.nextLine();
        project.setName(newName);
        project.setDescription(description);
    }

    public void showProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        String projectId = fingProjectIdByName(projectName);
        if (projectRepository.findAll().containsKey(projectId)) {
            System.out.println(projectRepository.findOne(projectId));
        } else {
            System.out.println("[NO SUCH PROJECT]");
        }
    }
}
