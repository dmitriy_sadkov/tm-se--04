package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public boolean isEmpty() {
        return projectMap.isEmpty();
    }

    public Map<String, Project> findAll() {
        return projectMap;
    }


    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void remove(String projectName) {
        projectMap.values().removeIf(nextProject -> nextProject.getName().equalsIgnoreCase(projectName));
    }

    public void removeAll() {
        projectMap.clear();
    }

    public Project findOne(String projectId) {
        return projectMap.get(projectId);
    }

    public void merge(Project project) {
        if (projectMap.containsValue(project)) {
            update(project.getId(), project.getName());
        } else {
            persist(project);
        }
    }

    private void update(String projectId, String projectName) {
        projectMap.get(projectId).setName(projectName);
    }
}
