package ru.sadkov.tm.menu;

public class Command {
    public static final String PROJECT_CREATE = "project-create";
    public static final String PCR = "pcr";

    public static final String PROJECT_LIST = "project-list";
    public static final String PL = "pl";

    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PR = "pr";

    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PCL = "pcl";

    public static final String TASK_CREATE = "task-create";
    public static final String TCR = "tcr";

    public static final String SHOW_ALL_TASK = "show-all-task";
    public static final String TL = "tl";

    public static final String TASK_REMOVE = "task-remove";
    public static final String TR = "tr";

    public static final String TASK_CLEAR = "task-clear";
    public static final String TCL = "tcl";

    public static final String TASKS_FOR_PROJECT = "tasks-for-project";
    public static final String TFP = "tfp";

    public static final String PROJECT_UPDATE = "project-update";
    public static final String PU = "pu";

    public static final String TASK_UPDATE = "task-update";
    public static final String TU = "tu";

    public static final String PROJECT_SHOW = "project-show";
    public static final String PS = "ps";

    public static final String TASK_SHOW = "task-show";
    public static final String TS = "ts";

    public static final String HELP = "help";
    public static final String EXIT = "exit";
}
