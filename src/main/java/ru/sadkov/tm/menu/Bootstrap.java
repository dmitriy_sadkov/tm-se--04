package ru.sadkov.tm.menu;

import ru.sadkov.tm.service.ProjectServise;
import ru.sadkov.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {
    private Scanner scanner = new Scanner(System.in);
    private ProjectServise projectService = new ProjectServise(scanner);
    private TaskService taskServise = new TaskService(projectService, scanner);
    private CommandRealization commander = new CommandRealization(taskServise, projectService, scanner);

    public void init() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        System.out.println("enter EXIT for exit");
        String command;
        do {
            command = scanner.nextLine().toLowerCase();
            switch (command) {
                case Command.PROJECT_CREATE:
                case Command.PCR: {
                    commander.createProject();
                    break;
                }
                case Command.PROJECT_REMOVE:
                case Command.PR: {
                    commander.removeProject();
                    break;
                }
                case Command.PROJECT_LIST:
                case Command.PL: {
                    commander.projectList();
                    break;
                }
                case Command.PROJECT_CLEAR:
                case Command.PCL: {
                    commander.projectClear();
                    break;
                }
                case Command.TASK_CREATE:
                case Command.TCR: {
                    commander.createTask();
                    break;
                }
                case Command.TASK_REMOVE:
                case Command.TR: {
                    commander.removeTask();
                    break;
                }
                case Command.SHOW_ALL_TASK:
                case Command.TL: {
                    commander.showTasks();
                    break;
                }
                case Command.TASK_CLEAR:
                case Command.TCL: {
                    commander.taskClear();
                    break;
                }
                case Command.HELP: {
                    commander.showHelp();
                    break;
                }
                case Command.TASKS_FOR_PROJECT:
                case Command.TFP: {
                    commander.showTasksForProject();
                    break;
                }
                case Command.PROJECT_UPDATE:
                case Command.PU: {
                    commander.updateProject();
                    break;
                }
                case Command.PROJECT_SHOW:
                case Command.PS: {
                    commander.showProject();
                    break;
                }
                case Command.TASK_UPDATE:
                case Command.TU: {
                    commander.updateTask();
                    break;
                }
                case Command.TASK_SHOW:
                case Command.TS: {
                    commander.showTask();
                    break;
                }
                case Command.EXIT: {
                    break;
                }

                default: {
                    System.out.println("[INCORRECT COMMAND]");
                }

            }
        } while (!command.equalsIgnoreCase("exit"));

    }
}
