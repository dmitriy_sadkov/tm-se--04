package ru.sadkov.tm.menu;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.service.ProjectServise;
import ru.sadkov.tm.service.TaskService;
import ru.sadkov.tm.util.ShowList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommandRealization {
    private TaskService taskService;
    private ProjectServise projectServise;
    private Scanner scanner;

    public CommandRealization(TaskService taskService, ProjectServise projectServise, Scanner scanner) {
        this.taskService = taskService;
        this.projectServise = projectServise;
        this.scanner = scanner;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public ProjectServise getProjectServise() {
        return projectServise;
    }

    public void setProjectServise(ProjectServise projectServise) {
        this.projectServise = projectServise;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        String projectName = scanner.nextLine();
        projectServise.saveProject(projectName);
    }

    public void removeProject() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        String projectName = scanner.nextLine();
        projectServise.removeProject(projectName);
        taskService.removeTaskForProject(projectName);
    }

    public void projectList() {
        System.out.println("[PROJECTS:]");
        projectServise.showProjects();
    }

    public void projectClear() {
        System.out.println("[PROJECTS CLEAR]");
        projectServise.clearProjects();
    }

    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        String taskName = scanner.nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = scanner.nextLine();
        taskService.saveTask(taskName, projectName);
    }

    public void removeTask() {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        String taskName = scanner.nextLine();
        taskService.removeTask(taskName);
    }

    public void showTasks() {
        System.out.println("[TASKS:]");
        taskService.showTasks();
    }

    public void taskClear() {
        System.out.println("[TASKS CLEAR]");
        taskService.clearTasks();
    }

    public void showHelp() {
        System.out.println("help: Show all commands");
        System.out.println("project-clear/pcl: Remove all project");
        System.out.println("project-remove/pr: Remove selected project");
        System.out.println("project-create/pcr: Create new projects");
        System.out.println("project-list/pl: Shaw all project");
        System.out.println("task-create/tcr: Create new task");
        System.out.println("task-list/tl: Show all tasks");
        System.out.println("task-remove/tr: Remove selected task");
        System.out.println("task-clear/tcl: Remove all Tasks");
        System.out.println("task-update/tu: Update task");
        System.out.println("task-show/ts: Show task");
        System.out.println("project-update/pu: Update project");
        System.out.println("project-show/ps: Show project");
    }

    public void showTasksForProject() {
        System.out.println("[TASKS FOR PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = scanner.nextLine();
        String projectId = projectServise.fingProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) {
            System.out.println("[INVALID PROJECT NAME]");
            return;
        }
        List<Task> result = new ArrayList<>();
        for (Task task : taskService.getTaskRepository().findAll().values()) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result.isEmpty()) {
            System.out.println("[NO TASKS IN PROJECT]");
        } else {
            ShowList.showList(result);
        }
    }

    public void updateProject() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = scanner.nextLine();
        projectServise.update(projectName);
    }

    public void showProject() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        String projectName = scanner.nextLine();
        projectServise.showProject(projectName);
    }

    public void updateTask() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = scanner.nextLine();
        taskService.update(taskName);
    }

    public void showTask() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = scanner.nextLine();
        taskService.showTask(taskName);
    }
}
